# Fast I2C EDID programmer

Command-line tool to read or write EDID binaries to EEPROM chips over I2C.

Performs 8-byte block accesses for added performance (supported on 24LC02B). To disable block access, you have to set `EDID_ADDR` to 1 in the source code.

As an alternative to this program, you can use `i2cdump -y (N) 0x50 [i]` to read your EDID. There are multiple shell scripts (also named `write-edid` 😉) based around i2c-tools to write EDID to a chip, but many of them call `i2cset` once per byte, which is slower than this program.

## Usage

```
USAGE:
    read (device num) [file path] = read EEPROM to terminal (and file if specified)
    write (device num) (file path) = reprogram EEPROM with file
```

## Compatibility

Working:

- [RP2040 (Pi Pico) I2C Interface](https://github.com/Nicolai-Electronics/rp2040-i2c-interface)
- VGA port on Ivy Bridge desktop

Not working: RTD2166 DP-to-VGA dongle (loads I2C from monitor on startup and presents a read-only interface)
