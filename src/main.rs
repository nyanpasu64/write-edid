use std::fs::File;
use std::process::exit;
use std::thread::sleep;
use std::time::Duration;

use anyhow::{Context, Result};
use i2cdev::core::*;
use i2cdev::linux::{LinuxI2CDevice, LinuxI2CError};
use nix::errno::Errno;

const EDID_ADDR: u16 = 0x50;

const DEBUG: bool = false;
const BLOCK_SIZE: usize = 8;

fn trim_rom(buffer: &[u8; 256]) -> Box<[u8]> {
    // Pre-HDMI (VGA/DVI) monitors have 128-byte EDID data. This can be mirrored across
    // both halves of the 256-byte address space (VX720 VGA), or the remainder can be
    // filled with FF (Dell DVI), or perhaps 00 as well?
    let head = &buffer[0..128];
    let tail = &buffer[128..256];
    let mut _128_bytes = false;

    if head == tail {
        _128_bytes = true;
    } else if tail.iter().all(|&x| x == 0xFF) {
        _128_bytes = true;
    }

    if _128_bytes {
        head.into()
    } else {
        buffer.as_ref().into()
    }
}

fn read_rom(dev: &mut LinuxI2CDevice) -> Result<Box<[u8]>> {
    let mut buffer = [0u8; 256];

    if BLOCK_SIZE > 1 {
        let mut addr: usize = 0;

        for chunk in buffer.chunks_mut(BLOCK_SIZE) {
            // https://github.com/rust-embedded/rust-i2cdev/issues/82
            let chunk_read = dev
                .smbus_read_i2c_block_data(addr.try_into().unwrap(), chunk.len() as u8)
                .context("reading I2C device")?;

            // We probably won't get short reads... probably? Who knows.
            // TODO what happens when we read 256 bytes from a a 128 byte EEPROM?
            chunk[0..chunk_read.len()].copy_from_slice(&chunk_read);
            addr += BLOCK_SIZE;
        }
    } else {
        for addr in 0..256 {
            let val = dev
                .smbus_read_byte_data(addr as u8)
                .context("reading I2C device")?;
            buffer[addr] = val;
        }
    }

    Ok(trim_rom(&buffer))
}

fn print_rom(dev: &mut LinuxI2CDevice) -> Result<Box<[u8]>> {
    if BLOCK_SIZE > 1 {
        let buffer: Box<[u8]> = read_rom(dev)?;

        for chunk in buffer.chunks(16) {
            for val in chunk {
                print!("{:02x} ", val);
            }
            println!();
        }
        println!();

        Ok(buffer)
    } else {
        let mut buffer = [0u8; 256];
        let mut addr: usize = 0;

        for _r in 0..16 {
            for _c in 0..16 {
                let val = dev
                    .smbus_read_byte_data(addr as u8)
                    .context("reading I2C device")?;
                print!("{:02x} ", val);
                buffer[addr] = val;
                addr += 1;
            }
            println!();
        }
        println!();

        Ok(trim_rom(&buffer))
    }
}

const WRITE_SLEEP: Duration = Duration::from_millis(5);
const WRITE_ATTEMPTS: usize = 4;

fn try_write<F: FnMut() -> Result<(), LinuxI2CError>>(mut write: F) -> Result<(), LinuxI2CError> {
    const ENXIO: i32 = Errno::ENXIO as i32;

    let mut result = Ok(());

    for attempt in 0..WRITE_ATTEMPTS {
        if attempt > 0 {
            if DEBUG {
                println!("- retry {}", attempt);
            }
            sleep(WRITE_SLEEP);
        }

        result = write();

        match result {
            // If we hit ENXIO, sleep and try writing again. Some EEPROMs take time to finish writing.
            Err(LinuxI2CError::Errno(ENXIO)) => continue,
            _ => return result,
        }
    }

    return result;
}

fn write_rom(dev: &mut LinuxI2CDevice, rom: &[u8]) -> Result<()> {
    println!("Writing ROM...");
    let mut addr: usize = 0;

    if BLOCK_SIZE > 1 {
        if DEBUG {
            println!("- block size {}", BLOCK_SIZE);
        }
        // https://datasheet.lcsc.com/lcsc/1809192119_Microchip-Tech-24LC02BT-I-SN_C5453.pdf
        // the chip can buffer at most 8 bytes of data at a time.
        for chunk in rom.chunks(BLOCK_SIZE) {
            if DEBUG {
                println!("writing to {}", addr);
            }
            // i don't understand i2c, but this works.
            // smbus_write_i2c_block_data() = I2C_SMBUS_I2C_BLOCK_DATA works,
            // smbus_write_block_data() = I2C_SMBUS_BLOCK_DATA rotates all bytes rightwards by 1.
            try_write(|| dev.smbus_write_i2c_block_data(addr.try_into().unwrap(), chunk))
                .with_context(|| format!("writing to address {}", addr))?;
            addr += BLOCK_SIZE;
        }
    } else {
        for &byte in rom {
            if DEBUG {
                println!("writing to {}", addr);
            }
            try_write(|| dev.smbus_write_byte_data(addr.try_into().unwrap(), byte))
                .with_context(|| format!("writing to address {}", addr))?;
            addr += 1;
        }
    }

    if DEBUG {
        println!("waiting for writes to finish...");
    }
    try_write(|| dev.smbus_read_byte_data(0).map(|_byte| ()))
        .context("waiting for writes to finish")?;

    Ok(())
}

fn help() {
    eprintln!("write-edid {}", env!("CARGO_PKG_VERSION"));
    eprintln!();
    eprintln!("USAGE:");
    eprintln!(
        "    read (device num) [file path] = read EEPROM to terminal (and file if specified)"
    );
    eprintln!("    write (device num) (file path) = reprogram EEPROM with file");
}

fn err() -> ! {
    help();
    exit(1)
}

fn open_device(device_n: &str) -> Result<LinuxI2CDevice> {
    let device_n = device_n.parse::<u32>().unwrap_or_else(|_| {
        eprintln!("Error: invalid device num {}, expected integer", device_n);
        eprintln!();
        err()
    });

    let dev_path = format!("/dev/i2c-{}", device_n);
    let dev = LinuxI2CDevice::new(dev_path, EDID_ADDR).context("LinuxI2CDevice::new()")?;

    Ok(dev)
}

fn main() -> Result<()> {
    use std::io::{Read, Write};

    let args: Vec<String> = std::env::args().collect();

    let command: &str = match args.get(1) {
        None => err(),
        Some(command) => command,
    };

    match command {
        "read" => {
            let device_n: &str = args.get(2).unwrap_or_else(|| err());
            let file_path: Option<&str> = args.get(3).map(|x| x.as_str());

            let mut dev = open_device(device_n)?;

            if let Some(file_path) = file_path {
                let rom: Box<[u8]> = read_rom(&mut dev)?;

                let mut file = File::create(file_path).with_context(|| file_path.to_owned())?;
                file.write_all(&*rom).context("writing file to disk")?;
                file.flush().context("flushing file to disk")?;
                drop(file);

                Ok(())
            } else {
                print_rom(&mut dev)?;

                Ok(())
            }
        }
        "write" => {
            let device_n: &str = args.get(2).unwrap_or_else(|| err());
            let file_path: &str = args.get(3).unwrap_or_else(|| err());

            let mut dev = open_device(device_n)?;

            let before = print_rom(&mut dev)?;

            // Write new ROM.
            let mut file = File::open(file_path).with_context(|| file_path.to_owned())?;
            let mut data: Vec<u8> = Vec::new();
            file.read_to_end(&mut data)
                .context("reading file from disk")?;
            drop(file);

            write_rom(&mut dev, &data)?;

            // Read ROM just written.
            let after = print_rom(&mut dev)?;

            if before == after {
                if before[0..data.len()] == data {
                    println!("Warning: wrote identical contents to ROM.");
                } else {
                    println!("Warning: ROM not modified, is write-protect enabled?");
                }
            }

            Ok(())
        }
        _ => err(),
    }
}
